# Runner with MySql

Découverte des services dans un runner

## Exercice

Vérifier les actions suivantes dans les logs de votre pipeline :
 - connexion au service de la base de données OK
 - tables créées
 - données bien insérées
 
